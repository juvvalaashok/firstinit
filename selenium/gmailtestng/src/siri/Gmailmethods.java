package siri;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Gmailmethods 
{
	public WebDriver driver;
	public String launch(String o,String d,String c)throws Exception
    {
		System.setProperty("webdriver.chrome.driver","D:\\softwares\\chromedriver_win32\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get(d);
		Thread.sleep(5000);
		return("done");
	}
	public String fill(String o,String d,String c)throws Exception
	{
		driver.findElement(By.xpath(o)).sendKeys(d);
		Thread.sleep(5000);
		return("done");
	}
	public String click(String o,String d,String c)throws Exception
	{
		driver.findElement(By.xpath(o)).click();
		Thread.sleep(5000);
		return("done");
	}
	public String validateuserid (String o,String d,String c) throws Exception
	{
		try
		{
			if(c.equals("valid") && driver.findElement(By.xpath("//*[@name='identifier']")).isDisplayed())
			{
				return("valid userid testpassed");
			}
			else if(c.equals("valid") && driver.findElement(By.xpath("(//*[contains(text(),' find your Google')])[2]")).isDisplayed())
			{
				return("invlaid userid test passed");
			}
			else if(c.equals("invalid") && driver.findElement(By.xpath("//*[contains(text(),'Enter an email or phone')]")).isDisplayed())
			{
				return("userid blank passed");
			}
			else
			{
				return("userid test failed");
			}
		  }
		catch(Exception e)
		{
			return("userid test was interupted");
		}
	}
	public String close(String o,String d,String c)throws Exception
	{
		driver.close();
		Thread.sleep(5000);
		return("done");
	}
	public String password(String o,String d,String c) throws Exception
	{
		try
		{
		if(c.equals("valid") && driver.findElement(By.xpath("//*[text()='COMPOSE']")).isDisplayed())
		{
			return("valid password test password");
	
		}
		else if(c.equals("invalid") && driver.findElement(By.xpath("//*[contains(text(),'Wrong password. Try again ')]")).isDisplayed())
		{
			return("wrong password test passed");
		}
		else if(c.equals("invalid") && driver.findElement(By.xpath("//*[text()='Enter a password']")).isDisplayed())
		{
			return("blank password test passed");
		}
		else
		{
			return("password test failed");
		}
		}
		catch(Exception e)
		{
			return("password test interupted");
		}
	}
			
		
	}


