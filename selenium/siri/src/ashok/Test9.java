package ashok;

import java.awt.Label;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.relevantcodes.extentreports.LogStatus;

public class Test9 
{
public static void main(String[] args) throws BiffException, IOException 
{
File f=new File("testdata.xls");
FileInputStream fis =new FileInputStream(f);
Workbook rwb=Workbook.getWorkbook(f);
Sheet rsh=rwb.getSheet(0);
int row=rsh.getRows();
WritableWorkbook wwb=Workbook.createWorkbook(f,rwb);
WritableSheet wsh=wwb.getSheet(0);
String res=null;
for(int i=1; i<row;i++)
{
String x=rsh.getCell(0,i).getContents();
String y=rsh.getCell(1,i).getContents();
String z=rsh.getCell(2,i).getContents();
String w=rsh.getCell(3,i).getContents();
System.setProperty("webdriver.chrome.driver","D:\\softwares\\chromedriver_win32\\chromedriver.exe");
ChromeDriver driver=new ChromeDriver();
driver.get("http://www.way2sms.com/content/index.html?");
Thread.sleep(5000);
driver.findElement(By.id("username")).sendKeys(x);
driver.findElement(By.name("password")).sendKeys(z);
driver.findElement(By.id("loginBTN")).click();
Thread.sleep(5000);
try{
if(x.length()==0 && ExpectedConditions.alertIsPresent()!=null)
{
res="test passed for blank username";
Label la=new Label(3,i,res);
	wsh.addCell(la);	
}
	
}
else if(x.length()<10 &&  ExpectedConditions.alertIsPresent()!=null)
{
	et.log(LogStatus.PASS,"test passed for wrong size mobile number");
}
else if(p.length()==0 && ExpectedConditions.alertIsPresent()!=null)
{
	et.log(LogStatus.PASS,"test passed for blank password");
}
else if(mc.equalsIgnoreCase("valid") && pc.equalsIgnoreCase("invalid")&& driver.findElement(By.xpath("//*[contains(text(),'Forgot Password')]")).isDisplayed())
{
	et.log(LogStatus.PASS,"test passed for invalid password");
	System.out.println("hai");
}
else if(mc.equalsIgnoreCase("valid") && pc.equalsIgnoreCase("valid") && driver.findElement(By.xpath("//*[text()='Send SMS']")).isDisplayed())
{
	et.log(LogStatus.PASS,"Test passed for valid data");
}
else if(mc.equalsIgnoreCase("invalid") && mc.equalsIgnoreCase("valid") && driver.findElement(By.xpath("//*[contains(text(),'looks like you haven`t')]")).isDisplayed())
{
	et.log(LogStatus.FAIL,"test passed for unregisterd number");
	System.out.println("how");
	
}
else {
	et.log(LogStatus.FAIL,"failed test case");
}
}
catch(Exception ex)
{
	et.log(LogStatus.ERROR,"Exception was raised");

}


}
