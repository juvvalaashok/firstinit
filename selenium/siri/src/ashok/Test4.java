package ashok;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Test4 {

	public static void main(String[] args) throws InterruptedException {
	ExtentReports er=new ExtentReports("gmailres.html",false);
	ExtentTest et=er.startTest("gmail home page test");
	System.setProperty("webdriver.chrome.driver","D:\\softwares\\chromedriver_win32\\chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.get("https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin");
	Thread.sleep(5000);
	driver.manage().window().maximize();
   try{
	if(driver.findElement(By.name("identifier")).isDisplayed())
	{
		et.log(LogStatus.PASS,"test passed");
	}
       }
	catch(Exception ex)
	{
	et.log(LogStatus.FAIL,"test failed");
	}
	driver.close();
	er.endTest(et);
	er.flush();
	
	}

}
